import { model } from "mongoose";
import playerSchema from "./playerSchema.js";
import studentSchema from "./studentschema.js";
export let Student = model("Student", studentSchema);
export let Player = model("Player", playerSchema);
